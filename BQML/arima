Google ARIMA Case (Google documentation based):

Add dataset here <>

LINK ARIMA OPTIONS: https://cloud.google.com/bigquery-ml/docs/reference/standard-sql/bigqueryml-syntax-create-time-series
LINK holiday_region: https://cloud.google.com/bigquery-ml/docs/reference/standard-sql/bigqueryml-syntax-create-time-series#holiday_region

// more information regarding holiday_region

#standardSQL
CREATE OR REPLACE MODEL `<>.ga_arima_model`
OPTIONS
  (model_type = 'ARIMA',
     time_series_timestamp_col = 'parsed_date',
     time_series_data_col = 'total_visits',
     auto_arima = TRUE,
     data_frequency='AUTO_FREQUENCY',
     holiday_region='DE' 
  ) AS
SELECT
  PARSE_TIMESTAMP("%Y%m%d", date) AS parsed_date,
  SUM(totals.visits) AS total_visits
FROM
  `bigquery-public-data.google_analytics_sample.ga_sessions_*`
GROUP BY date

// Evaluate

#standardSQL
SELECT
 *
FROM
 ML.EVALUATE(MODEL `<>.ga_arima_model`)

// Coefficient

#standardSQL
SELECT
 *
FROM
 ML.ARIMA_COEFFICIENTS(MODEL `<>.ga_arima_model`)


// FORECAST (horizon = forecast periods (here 30 days))

#standardSQL
SELECT
 *
FROM
 ML.FORECAST(MODEL `<>.ga_arima_model`,
             STRUCT(30 AS horizon, 0.8 AS confidence_level))


// Forecast (dataviz in datastudio)

#standardSQL
SELECT
 history_timestamp AS timestamp,
 history_value,
 NULL AS forecast_value,
 NULL AS prediction_interval_lower_bound,
 NULL AS prediction_interval_upper_bound
FROM
 (
   SELECT
     PARSE_TIMESTAMP("%Y%m%d", date) AS history_timestamp,
     SUM(totals.visits) AS history_value
   FROM
     `bigquery-public-data.google_analytics_sample.ga_sessions_*`
   GROUP BY date
   ORDER BY date ASC
 )
UNION ALL
SELECT
 forecast_timestamp AS timestamp,
 NULL AS history_value,
 forecast_value,
 prediction_interval_lower_bound,
 prediction_interval_upper_bound
FROM
 ML.FORECAST(MODEL `<>.ga_arima_model`,
             STRUCT(30 AS horizon, 0.8 AS confidence_level))

