﻿___INFO___

{
  "type": "TAG",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "Plausible Analytics",
  "brand": {
    "id": "brand_dummy",
    "displayName": ""
  },
  "description": "",
  "containerContexts": [
    "SERVER"
  ]
}


___TEMPLATE_PARAMETERS___

[
  {
    "type": "TEXT",
    "name": "endpointUrl",
    "displayName": "Endpoint Url",
    "simpleValueType": true,
    "valueValidators": [
      {
        "type": "NON_EMPTY"
      }
    ],
    "defaultValue": "https://plausible.io/api/event"
  },
  {
    "type": "PARAM_TABLE",
    "name": "paramTable1",
    "displayName": "",
    "paramTableColumns": []
  }
]


___SANDBOXED_JS_FOR_SERVER___

const getAllEventData = require("getAllEventData");
const sendHttpRequest = require('sendHttpRequest'); 
const setResponseStatus = require("setResponseStatus");
const JSON = require('JSON');
const setResponseHeader = require("setResponseHeader");
const decodeUriComponent = require("decodeUriComponent");
//const logToConsole = require("logToConsole");

const evData = getAllEventData();
var url = evData.page_location || "";
var ref = evData.page_referrer || null; 

url = url.replace('https://demo.gmp-masterclass.de/', 'https://plausible.io/pioneers_test/'); 
url = url.replace("?", "").replace('=', "_");
if (ref) ref = ref.replace('https://demo.gmp-masterclass.de/', 'https://plausible.io/'); 

const ep = data.endpointUrl;

var payload = {  
  n : "pageview", 
  u : url, 
  d : null,  
  r : ref, 
  w : (evData.screen_resolution || "0x0").split('x')[0] || 0 
}; 


const postBody = JSON.stringify(payload);
// Sends a POST request and nominates response based on the response to the POST
// request.
sendHttpRequest(ep, (statusCode, headers, body) => {
  setResponseStatus(statusCode);
  setResponseHeader('cache-control', headers['cache-control']);
}, {headers: {'user-agent': decodeUriComponent(evData.user_agent),
          'content-type': 'text/plain',       
          'accept-language': decodeUriComponent(evData.language || ""),
          'x-forwarded-for': decodeUriComponent(evData.ip_override)}, method: 'POST', timeout: 500}, postBody);


// data.gtmOnSuccess aufrufen, wenn das Tag fertig ist.
data.gtmOnSuccess();


___SERVER_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "read_event_data",
        "versionId": "1"
      },
      "param": [
        {
          "key": "eventDataAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "access_response",
        "versionId": "1"
      },
      "param": [
        {
          "key": "writeResponseAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        },
        {
          "key": "writeHeaderAccess",
          "value": {
            "type": 1,
            "string": "specific"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "send_http",
        "versionId": "1"
      },
      "param": [
        {
          "key": "allowedUrls",
          "value": {
            "type": 1,
            "string": "any"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  }
]


___TESTS___

scenarios: []


___NOTES___

Created on 3.11.2021, 17:39:39


