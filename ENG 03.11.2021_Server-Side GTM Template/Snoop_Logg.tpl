﻿___INFO___

{
  "type": "CLIENT",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "Snoop Logg",
  "brand": {
    "id": "brand_dummy",
    "displayName": ""
  },
  "description": "",
  "containerContexts": [
    "SERVER"
  ]
}


___TEMPLATE_PARAMETERS___

[]


___SANDBOXED_JS_FOR_SERVER___

const getRequestPath = require('getRequestPath');
const getRequestQueryParameter  = require('getRequestQueryParameter');
const claimRequest = require('claimRequest');
const runContainer = require("runContainer");
const setPixelResponse = require('setPixelResponse'); 
const returnResponse = require('returnResponse'); 
const logToConsole = require('logToConsole'); 

const rp = getRequestPath();
logToConsole('Hi There');

if (rp === "/snooplogg") {
  claimRequest();
  
  var url = getRequestQueryParameter ('url');
  var ref = getRequestQueryParameter ('ref');
  var cid = getRequestQueryParameter ('cid') || 1000;
  
  var evData = {
    page_location: url,
    page_referrer: ref,
    client_id: cid,
    ip_override: require("getRemoteAddress")(),
    user_agent: require("getRequestHeader")('user-agent'),
    event_name: "page_view"
  };
  
  runContainer(evData, () => {   
    setPixelResponse(); 
    returnResponse();     
  });    
    
}


___SERVER_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "read_request",
        "versionId": "1"
      },
      "param": [
        {
          "key": "requestAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        },
        {
          "key": "headerAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        },
        {
          "key": "queryParameterAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "return_response",
        "versionId": "1"
      },
      "param": []
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "access_response",
        "versionId": "1"
      },
      "param": [
        {
          "key": "writeResponseAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        },
        {
          "key": "writeHeaderAccess",
          "value": {
            "type": 1,
            "string": "specific"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "run_container",
        "versionId": "1"
      },
      "param": []
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "logging",
        "versionId": "1"
      },
      "param": [
        {
          "key": "environments",
          "value": {
            "type": 1,
            "string": "debug"
          }
        }
      ]
    },
    "isRequired": true
  }
]


___TESTS___

scenarios: []


___NOTES___

Created on 3.11.2021, 17:39:20


