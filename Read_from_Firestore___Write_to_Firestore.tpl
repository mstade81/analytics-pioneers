﻿___INFO___

{
  "type": "MACRO",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "Read from Firestore / Write to Firestore",
  "description": "",
  "containerContexts": [
    "SERVER"
  ]
}


___TEMPLATE_PARAMETERS___

[
  {
    "type": "TEXT",
    "name": "fsPath",
    "displayName": "Firestore Document Path",
    "simpleValueType": true,
    "valueValidators": [
      {
        "type": "NON_EMPTY"
      }
    ]
  },
  {
    "type": "TEXT",
    "name": "projectId",
    "displayName": "Project ID",
    "simpleValueType": true,
    "valueValidators": [
      {
        "type": "NON_EMPTY"
      }
    ]
  },
  {
    "type": "TEXT",
    "name": "keypath",
    "displayName": "Key Path",
    "simpleValueType": true,
    "valueValidators": [
      {
        "type": "NON_EMPTY"
      }
    ]
  },
  {
    "type": "TEXT",
    "name": "writeval",
    "displayName": "Write value if not found",
    "simpleValueType": true,
    "valueValidators": [
      {
        "type": "NON_EMPTY"
      }
    ]
  }
]


___SANDBOXED_JS_FOR_SERVER___

const Firestore = require('Firestore');
const doc = data.fsPath;
const keypath = data.keypath;
const writeval = data.writeval;

var ts; var ua; var input = {};

input[keypath] = writeval;


function writeNew(d, p) {
  return Firestore.write(doc, input, {
    projectId: p,
    merge: true,
  }).then(function(result){return undefined;}, function(){ return undefined;});  
}

//Lesen aus Firestore 
return Firestore.read(doc, {
  projectId: data.projectId,
  disableCache : true,
})
  .then(
    function(result)
    { 
     if (result.data[keypath] === undefined){
      writeNew(doc, data.projectId);return(undefined);}
      else {return result.data[keypath];}
     },
  function(result)
    {writeNew(doc, data.projectId); return(undefined);}
);


___SERVER_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "access_firestore",
        "versionId": "1"
      },
      "param": [
        {
          "key": "allowedOptions",
          "value": {
            "type": 2,
            "listItem": [
              {
                "type": 3,
                "mapKey": [
                  {
                    "type": 1,
                    "string": "projectId"
                  },
                  {
                    "type": 1,
                    "string": "path"
                  },
                  {
                    "type": 1,
                    "string": "operation"
                  }
                ],
                "mapValue": [
                  {
                    "type": 1,
                    "string": "*"
                  },
                  {
                    "type": 1,
                    "string": "*"
                  },
                  {
                    "type": 1,
                    "string": "read_write"
                  }
                ]
              }
            ]
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  }
]


___TESTS___

scenarios: []


___NOTES___

Created on 29.3.2022, 16:28:33


