--- UNNEST FIELDS ---



SELECT
  event_name,
  params.key AS event_parameter_key,
  CASE
    WHEN params.value.string_value IS NOT NULL THEN 'string'
    WHEN params.value.int_value IS NOT NULL THEN 'int'
    WHEN params.value.double_value IS NOT NULL THEN 'double'
    WHEN params.value.float_value IS NOT NULL THEN 'float'
END
  AS event_parameter_value
FROM
 `<insert your BQ Dataset>.events_*`,
 UNNEST(event_params) AS params
WHERE
  _table_suffix BETWEEN '20201019'
  AND FORMAT_DATE('%Y%m%d',DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))
GROUP BY
  1,
  2,
  3
ORDER BY
  1,
  2



--- FUNNEL ---



WITH date_range AS(
  SELECT 
    *
  FROM `<insert your BQ Dataset>.events_*`
  WHERE
    _table_suffix BETWEEN 
    '20201019'
    #FORMAT_DATE('%Y%m%d',DATE_SUB(CURRENT_DATE(), INTERVAL 31 DAY))
    AND 
    FORMAT_DATE('%Y%m%d',DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))
)

SELECT
product,
funnel,
#event_name,
COUNT(product) as user_count, -- Anzahl produkt
COUNT(product) / lag(COUNT(product)) OVER (ORDER BY funnel) as ratio_previous_step, -- Anzahl podukt funnel / Anzahl Produkt funnel der Zeile darüber (lag = eine Zeile nach oben springen, wenn nach funnel sortiert wird)


FROM(

SELECT
  user_pseudo_id,
  CASE
  WHEN (event_name = "view_item") THEN "1. PDP"
  WHEN (event_name = "add_to_cart") THEN "2. Add to Basket"
  WHEN (event_name = "begin_checkout") THEN "3. Checkout"
  WHEN (event_name = "purchase") THEN "4. Thank You"
  ELSE "NOT DEFINED"
END AS funnel,
  
  --User journey nach Funnel zeitlich geordnet
  ROW_NUMBER() OVER (PARTITION BY user_pseudo_id order by event_timestamp ) rank,
  
  -- event_date (dimension | Datum des Events)
  PARSE_DATE('%Y%m%d', event_date) AS event_date,
 
 -- event_timestamp (dimension | Zeit (in microseconds, UTC) wann der event des clients erfasst wurde)
  TIMESTAMP_MICROS(event_timestamp) AS event_timestamp,
 
 -- event_string_value (dimension | string value des event parameters)
  (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'page_location') AS page_location,

 -- Produktname aus UNNEST (item) 
  item_name as product,
  
 -- Transaction ID 
  ecommerce.transaction_id,
  
FROM date_range, UNNEST (items)
WHERE 
 item_name = "Beanie" AND
 (
 event_name = "view_item" OR
 event_name = "add_to_cart" OR
 event_name = "begin_checkout" OR
 event_name = "purchase"
 )
ORDER BY
 user_pseudo_id,
 event_date,
 event_timestamp
 )
 GROUP BY 1,2 --nach produkt und funnel gruppiert
 ORDER BY 
 product, funnel



--- GA4 - Sessions, Engaged Sessions, Bounce ---

WITH date_range AS(
SELECT
  *
FROM
  `<insert your BQ Dataset>.events_*`
),
engagement AS (
SELECT
    count(DISTINCT CASE WHEN session_engaged = '1' THEN concat(user_pseudo_id,session_id) END) AS engaged_sessions,
    sum(engagement_time_msec)/1000 as engagement_time_seconds,
    count(DISTINCT CASE WHEN session_engaged = '0' THEN concat(user_pseudo_id,session_id) END) AS bounces
  FROM (
    SELECT
      user_pseudo_id,
      (SELECT value.int_value FROM unnest(event_params) WHERE key = 'ga_session_id') AS session_id,
      max((SELECT value.string_value FROM unnest(event_params) WHERE key = 'session_engaged')) AS session_engaged,
      max((SELECT value.int_value FROM unnest(event_params) WHERE key = 'engagement_time_msec')) AS engagement_time_msec
    FROM
            `<insert your BQ Dataset>.events_*`
    GROUP BY
      user_pseudo_id,
      session_id))
      
  -- main query
SELECT
  -- sessions (metric | the total number of sessions)
  count(DISTINCT CASE WHEN event_name = 'session_start' THEN concat(user_pseudo_id, cast(event_timestamp AS string)) END) AS sessions,
  
  -- engaged sessions (metric | the number of sessions that lasted longer than 10 seconds, or had a conversion event, or had 2 or more screen or page views)
  max(engaged_sessions) AS engaged_sessions,
  
  -- engagement rate (metric | the percentage of engaged sessions compared to all sessions)
  safe_divide(max(engaged_sessions),count(DISTINCT CASE WHEN event_name = 'session_start' THEN concat(user_pseudo_id, cast(event_timestamp AS string)) END)) AS engagement_rate,
  
  -- engagement time (metric | the average length of time in seconds that the app was in the foreground, or the web site had focus in the browser)
  safe_divide(max(engagement_time_seconds),max(engaged_sessions)) AS engagement_time,
  
  -- bounces (metric | the total number of non-engaged sessions)
  max(bounces) AS bounces,
  
  -- bounce rate (metric | bounces divided by total sessions)
  safe_divide(max(bounces),count(DISTINCT CASE WHEN event_name = 'session_start' THEN concat(user_pseudo_id, cast(event_timestamp AS string)) END)) AS bounce_rate,
  
  -- event count per session (metric | number of times an individual event (i.e. 'page_view') was triggered divided by all sessions)
  safe_divide(count(DISTINCT CASE WHEN event_name = 'page_view' THEN concat(user_pseudo_id, cast(event_timestamp AS string)) END),count(DISTINCT CASE WHEN event_name = 'session_start' THEN concat(user_pseudo_id, cast(event_timestamp AS string)) END)) AS event_count_per_session
FROM date_range, engagement

